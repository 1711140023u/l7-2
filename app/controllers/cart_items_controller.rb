class CartItemsController < ApplicationController
        
    def create
     @products = Product.all
     product = Product.find(params[:product_id])
     cart = current_cart
     cart_item = CartItem.new(product_id: product,cart_id: cart)
     cart_item.save
     redirect_to '/'
    end
    
    
    def destroy
     @carts = Cart.all
     cart = CartItem.find(params[:product_id])
     product = Product.find_by(name: session[:cart_id])
     cart.cart_items.find_by(product_id: product.id).destroy
     redirect_to '/'
    end
end
